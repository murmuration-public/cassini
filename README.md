# Use cases Notebooks

This repository contains Jupyter Notebooks to demonstrate data usage with Murmuration's Indicators.

These notebooks were developped by Murmuration for 2022 Cassini Hackathon participants.

## Murmuration
Murmuration provides solutions to measure the environmental impact of tourism. Murmuration develops a system for calculating environmental indicators dedicated to tourism and offering different levels of services for institutions in charge of managing tourism activity. 
These indicators are calculated from satellite earth observation data, mainly provided by the Copernicus program, combined with environmental data, in-situ observation data, open statistical data and business data. They cover different themes: air and atmosphere, water, biodiversity and vegetation, soils and quantification of tourism activity.

## Cassini Hackathon Tutorials
Murmuration provides 6 indicators, with a Jupyter Notebooks demonstrating the indicator usage: 
- [Air Quality](Air_Quality_Cassini_tutorial.ipynb)
- [Aquatic Coverage](Aquatic Coverage.ipynb)
- [Sea Water Quality](Sea_Water_Quality_Cassini_tutorial.ipynb)
- [Touristic Infrastructures](Tourism_Infrastructure.ipynb)
- [Urbanisation](Urbanisation_Cassini_tutorial.ipynb)
- [Vegetation Health](Vegetation_Health_Cassini_tutorial.ipynb)

## Contact
Support will be provided during the Hackathon event on-site in France.
Murmuration's team will also be available on Discord, reach out to : Camille Lainé, Murugesh Manthiramoorthi, Emma Rizzi

For further queries, contact us: support@murmuration-sas.com
